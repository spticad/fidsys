package ru.mremne.service;

/**
 * autor:maksim
 * date: 01.04.15
 * time: 17:02.
 */
public interface FidService {
    public boolean addAngles(Double[] angles);
    public boolean checkAngles(Double[] angles);

}
